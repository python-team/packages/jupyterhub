# Setting up Jupyterhub

This is a work-in-progress documentation on how to set up Jupyterhub
using the Debian packages.

## Setting up HTTPS

If your Jupyterhub instance is meant to be public, you really need to
set up HTTPS so that passwords do not fly over the network in
cleartext.  Several setups are possible.

### HTTPS is managed outside of Jupyterhub

For instance, you have an Apache or Nginx reverse-proxy. In this
situation, no particular configuration is required for Jupyterhub, but
you need to obtain certificates and configure the reverse-proxy to use
them. A simple way is to get certificates from Let's Encrypt using
Certbot. Install the `certbot` package as well as
`python3-certbot-apache` (or `python3-certbot-nginx`), then run
`certbot --apache` (or `certbot --nginx`) and follow the instructions.

More detailed instructions can be found at https://certbot.eff.org/
(you can skip the part about installing Certbot with Snap since Debian
provides packages).

### HTTPS is handled by Jupyterhub directly.

This involves two steps:

1. Creating or obtaining SSL/TLS certificates: as above, Certbot can
   be of use. Run `certbot --apache certonly` or `certbot --nginx
   certonly` to get certificates.

2. Configuring Jupyterhub to use the certificates: edit the
   `/etc/jupyterhub/jupyterhub_config.py` file, and uncomment and fill
   the lines about `c.JupyterHub.ssl_cert` and
   `c.JupyterHub.ssl_key`. If you used Certbot, then the appropriate
   values are `/etc/letsencrypt/live/<yourhostname>/fullchain.pem` and
   `/etc/letsencrypt/live/<yourhostname>/privkey.pem` respectively.

You need to restart Jupyterhub, with `systemctl restart jupyterhub`.
